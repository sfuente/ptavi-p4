#!/usr/bin/python3
# -*- coding: utf-8 -*-
import socketserver
import sys
import json
import time


from src import server
import unittest
import unittest.mock as mock

class Test_SIPRegisterHandler(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.Users = {}

    @staticmethod
    def json2registered():
        return server.SIPRegisterHandler.json2registered
    def wfile():
        return server.SIPRegisterHandler.wfile
    def rfile():
        return server.SIPRegisterHandler.rfile

    def write(self):
        return self.write
    setattr(wfile, 'write', write)
    # setattr(bytes, 'write', write)



    def test_register2json(self):
        self.Users = {
            "luke@polismassa.com": {
                "address": "127.0.0.1",
                "expire": "2021-05-11 21:31:16 +0000"
            }
        }
        server.SIPRegisterHandler.register2json(self)
        try:
            with open('registered.json', 'r') as infile:
                self.assertEqual(self.Users, json.loads(infile))
        except:
            pass

    def test_check_expiration(self):
        self.Users = {
            "luke@polismassa.com": {
                "address": "127.0.0.1",
                "expire": "2021-05-11 21:31:16 +0000"
            },
            "leia@polismassa.com": {
                "address": "127.0.0.1",
                "expire": "2021-05-11 21:31:29 +0000"
            },
            "han@polismassa.com": {
                "address": "127.0.0.1",
                "expire": "2002-05-11 21:31:35 +0000"
            }
        }
        time = '2015-21-10 07:28:00 +0000'
        mockUsers = {
            "luke@polismassa.com": {
                "address": "127.0.0.1",
                "expire": "2021-05-11 21:31:16 +0000"
            },
            "leia@polismassa.com": {
                "address": "127.0.0.1",
                "expire": "2021-05-11 21:31:29 +0000"
            }
        }
        server.SIPRegisterHandler.check_expiration(self, time)
        self.assertEqual(self.Users, mockUsers)

    def test_json2registered(self):
        mockUsers = {}
        try:
            with open('registered.json', 'r') as infile:
                mockUsers = json.loads(infile)
        except:
            pass
        server.SIPRegisterHandler.json2registered(self)
        self.assertEqual(self.Users, mockUsers)

    def test_handle(self):
        self.Users = None
        # server.SIPRegisterHandler.handle(self)

if __name__ == "__main__":
    unittest.main()

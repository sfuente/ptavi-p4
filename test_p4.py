import os
import signal
import threading
import sys
import time
import subprocess

RESULT_ERROR = '----------ERROR---------'
RESULT_OK = '----------OK---------'

init_server = lambda : subprocess.run('python3 src/server.py 8080', shell=True, check=True)
init_client = lambda : subprocess.run('python3 src/client.py localhost 8080 register luke@polismassa.com 3600', shell=True, check=True)

def test_p4():
    try:
        threads = []
        server_thread = threading.Thread(target=init_server)
        server_thread.daemon = True
        server_thread.start()
        threads.append(server_thread)
    except:
        raise SystemExit

    try:
        client_thread = threading.Thread(target=init_client)
        client_thread.daemon = True
        time.sleep(1)
        client_thread.start()
        threads.append(client_thread)
        time.sleep(1)
        print(os.getpid())
        raise_sigint()
    except SystemExit:
       raise Exception


def raise_sigint():
    if hasattr(signal, 'CTRL_C_EVENT'):
        #  WINDOWS
        os.kill(os.getpid(), signal.CTRL_C_EVENT)
    else:
        # UNIX
        pgid = os.getpgid(os.getpid())
        if pgid == 1:
            os.kill(os.getpid(), signal.SIGINT)
        else:
            os.killpg(os.getpgid(os.getpid()), signal.SIGINT)

def register_state(result):
    try:
        with open('result_test.txt', 'w') as infile:
            infile.write(result)
    except:
        pass

if __name__ == "__main__":
    try:
        test_p4()
        register_state(RESULT_OK)
                        
    except KeyboardInterrupt:
        print('KeyboardInterrupt')
        

